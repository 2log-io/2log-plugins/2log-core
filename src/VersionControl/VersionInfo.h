#ifndef VERSIONINFO_H
#define VERSIONINFO_H

#include <QObject>
#ifndef QH_VERSION
#define QH_VERSION "0.0.0"
#endif

#ifndef QH_BUILD
#define QH_BUILD "local-build"
#endif


class VersionInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString build READ commitSha CONSTANT)
    Q_PROPERTY(QString version READ versionString CONSTANT)

public:
    explicit VersionInfo(QObject *parent = nullptr);

    QString commitSha() const;
    QString versionString() const;

signals:
private:
    QString _commitSha = QH_BUILD;
    QString _versionString = QH_VERSION;

};

#endif // VERSIONINFO_H
