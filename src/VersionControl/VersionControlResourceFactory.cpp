#include "VersionControlResourceFactory.h"
#include "Database/UserAccess.h"
#include "Server/Resources/ObjectResource/QObjectResource.h"

VersionControlResourceFactory::VersionControlResourceFactory(QObject *parent)
    : IResourceFactory(parent)
{
}

QString VersionControlResourceFactory::getResourceType() const
{
    return "object";
}

QString VersionControlResourceFactory::getDescriptorPrefix() const
{
    return "version";
}

resourcePtr VersionControlResourceFactory::createResource(QString token, QString descriptor, QObject *parent)
{
    iIdentityPtr user = AuthenticationService::instance()->validateToken(token);
    if(user.isNull()){
        return resourcePtr();
    }

    if(!( user->isAuthorizedTo(LAB_ADMIN) || user->isAuthorizedTo(IS_ADMIN))){
        return resourcePtr();
    }

    return resourcePtr(new QObjectResource(&mInfo));
}
