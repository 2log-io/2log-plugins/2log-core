#include "VersionInfo.h"

VersionInfo::VersionInfo(QObject *parent)
    : QObject{parent}
{

}

QString VersionInfo::commitSha() const
{
    return _commitSha;
}

QString VersionInfo::versionString() const
{
    return _versionString;
}
