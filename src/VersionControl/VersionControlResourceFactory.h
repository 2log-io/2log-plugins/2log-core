#ifndef VERSIONCONTROLRESOURCEFACTORY_H
#define VERSIONCONTROLRESOURCEFACTORY_H

#include <QObject>
#include "Server/Resources/ResourceManager/IResourceFactory.h"
#include "VersionInfo.h"

class VersionControlResourceFactory : public IResourceFactory
{
    Q_OBJECT

public:
    explicit VersionControlResourceFactory(QObject *parent = nullptr);
    virtual ~VersionControlResourceFactory() override {};
    virtual QString getResourceType() const override;
    virtual QString getDescriptorPrefix() const override;


private:
    VersionInfo mInfo;
    virtual resourcePtr createResource( QString token, QString descriptor, QObject* parent = nullptr) override;


signals:

};

#endif // VERSIONCONTROLRESOURCEFACTORY_H
