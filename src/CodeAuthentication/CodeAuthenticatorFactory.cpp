#include "CodeAuthenticatorFactory.h"
#include "CodeAuthenticator.h"

CodeAuthenticationFactory::CodeAuthenticationFactory(CodeAuthenticationController* controller, QObject *parent)
    : IResourceFactory{parent},
      _controller(controller)
{
}


QString CodeAuthenticationFactory::getResourceType() const
{
    return "object";
}

QString CodeAuthenticationFactory::getDescriptorPrefix() const
{
    return "codeAuthenticator";
}

resourcePtr CodeAuthenticationFactory::createResource(QString token, QString descriptor, QObject *parent)
{
    Q_UNUSED(descriptor)
    Q_UNUSED(parent)

    iIdentityPtr user = AuthenticationService::instance()->validateToken(token);
    if(user.isNull())
        return nullptr;

    QStringList tokens = descriptor.split("/");

    CodeAuthenticator* authenticator;
    if(tokens.count() == 2)
    {
        bool ok;
        quint8 codeLength = tokens.at(1).toUInt(&ok);
        if(ok)
        {
            authenticator = new CodeAuthenticator(codeLength);
            _controller->registerAuthenticator(authenticator);
            return resourcePtr(authenticator);
        }
    }

    authenticator = new CodeAuthenticator();
    _controller->registerAuthenticator(authenticator);
    return resourcePtr(authenticator);
}
