#ifndef CODEAUTHENTICATIONCONTROLLER_H
#define CODEAUTHENTICATIONCONTROLLER_H

#include <QObject>
#include <QPointer>
#include "Server/Services/IService.h"
#include <QTimer>
class CodeAuthenticator;
class CodeAuthenticationController : public IService
{
    Q_OBJECT

public:
    explicit CodeAuthenticationController(QObject *parent = nullptr);

    void registerAuthenticator(CodeAuthenticator* authenticator);
    virtual QStringList     getServiceCalls() const override;
    virtual QString         getServiceName() const override;
    virtual bool            call(QString call, QString token, QString cbID, QVariant argument = QVariant()) override;

private:
    QMap <QPointer<CodeAuthenticator>, QString> _authenticators;
    QMap <QString, QPointer<CodeAuthenticator>> _codes;
    void removeAuthenticator(CodeAuthenticator *authenticator);
    void renewCode(CodeAuthenticator* authenticator);
    QTimer _timer;
    QString _prefix;

    static QString createID(int idLength = 32);


signals:

};

#endif // CODEAUTHENTICATIONCONTROLLER_H

