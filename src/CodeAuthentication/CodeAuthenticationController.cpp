#include "CodeAuthenticationController.h"
#include "CodeAuthenticator.h"
#include "Server/Settings/SettingsManager.h"
#include <QPointer>
#include <QUuid>

CodeAuthenticationController::CodeAuthenticationController( QObject *parent)
    : IService{parent}
{
    connect(&_timer, &QTimer::timeout, this, [=]()
    {
        QMapIterator<QPointer<CodeAuthenticator>, QString> it(_authenticators);
        while(it.hasNext())
        {
            it.next();
            auto authenticator = it.key();
            renewCode(authenticator);
        }
    });

    _prefix = SettingsManager::instance()->getSetting("authentication","codePrefix","https://2log.app/").toString();
    if(!_prefix.endsWith("/"))
        _prefix.append("/");
    int codeRenewal = SettingsManager::instance()->getSetting("authentication","codeRenewal",30000).toInt();
    _timer.setInterval(codeRenewal);
    _timer.start();
}

void CodeAuthenticationController::registerAuthenticator(CodeAuthenticator *authenticator)
{
    auto objectPointer = QPointer<CodeAuthenticator>(authenticator);

    if(!_authenticators.contains(objectPointer))
    {
        connect(objectPointer.data(), &QObject::destroyed, objectPointer.data(), [=](QObject* object)
        {
            CodeAuthenticator* data = reinterpret_cast<CodeAuthenticator*>(object);
            removeAuthenticator(data);
        });
    }

    renewCode(authenticator);
}

QStringList CodeAuthenticationController::getServiceCalls() const
{
    return QStringList{"authenticate"};
}

QString CodeAuthenticationController::getServiceName() const
{
    return "codeAuthenticator";
}

bool CodeAuthenticationController::call(QString call, QString token, QString cbID, QVariant argument)
{
    if(call == "authenticate")
    {
        QVariantMap answer;
        QString code = argument.toMap()["code"].toString();
        auto authenticator = _codes.value(code);
        if(authenticator.isNull())
        {
            removeAuthenticator(authenticator);
            answer["errcode"] = -1;
            Q_EMIT response(cbID, answer);
            return true;
        }


        authenticator->userAuthenticated(token);
        answer["errcode"] = -0;
        Q_EMIT response(cbID, answer);
        return true;
    }
    return false;
}

void CodeAuthenticationController::removeAuthenticator(CodeAuthenticator* authenticator)
{
    QString code = _authenticators.value(authenticator);
    _authenticators.remove(authenticator);
    _codes.remove(code);
    if(_codes.count() != _authenticators.count())
    {
        qWarning()<<Q_FUNC_INFO;
        qWarning()<<"Codes:"<<_codes.count();
        qWarning()<<"Auth:"<<_authenticators.count();
    }
}

void CodeAuthenticationController::renewCode(CodeAuthenticator* authenticator)
{
    if(authenticator == nullptr)
    {
        removeAuthenticator(authenticator);
        return;
    }

    QString oldCode = _authenticators.value(authenticator, "");
    if(!oldCode.isEmpty())
        _codes.remove(oldCode);

    QString newCode;
    if(authenticator->codeLength() > 0)
    {
        qint16 restLength = authenticator->codeLength() - _prefix.length();
        if(restLength >= 4)
        {
            do
            {
                newCode = _prefix  + createID(restLength);
            }
            while((_codes.contains(newCode)));
        }
        else
        {
            do
            {
                newCode = createID(authenticator->codeLength() );
            }
            while((_codes.contains(newCode)));
        }
    }
    else
    {
        do
        {
            newCode = _prefix + createID();
        }
        while((_codes.contains(newCode)));
    }

    _codes.insert(newCode, authenticator);
    _authenticators.insert(authenticator, newCode);
    authenticator->setCode(newCode);

    if(_codes.count() != _authenticators.count())
    {
        qWarning()<<Q_FUNC_INFO;
        qWarning()<<"Codes:"<<_codes.count();
        qWarning()<<"Auth:"<<_authenticators.count();
    }

}

QString CodeAuthenticationController::createID(int idLength)
{
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
    const int randomStringLength = idLength; // assuming you want random strings of 12 characters

    QString randomString;
    for(int i=0; i<randomStringLength; ++i)
    {
        int index = qrand() % possibleCharacters.length();
        QChar nextChar = possibleCharacters.at(index);
        randomString.append(nextChar);
    }
    return randomString;
}
