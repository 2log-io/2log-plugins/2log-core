#include "CodeAuthenticator.h"
#include "CodeAuthenticationController.h"
#include "Server/Authentication/AuthentificationService.h"

CodeAuthenticator::CodeAuthenticator(quint16 codelength, QObject *parent)
    : ObjectResource{nullptr, parent},
    _codeLength(codelength)
{
    setDynamicContent(true);
}

CodeAuthenticator::CodeAuthenticator(QObject *parent)  : ObjectResource{nullptr, parent},
    _codeLength(-1)
{
    setDynamicContent(true);
}


void CodeAuthenticator::userAuthenticated( QString token)
{
    QVariantMap data;
    auto user = AuthenticationService::instance()->validateToken(token);
    if(user.isNull())
    {
        data["errorCode"]  =  -1;
        data["errorString"]  = "Unknown token";
    }
    else
    {
        data["errorCode"]  =  0;
        data["id"] = user->identityID();
    }

    qDebug()<<data;
    Q_EMIT sendEvent(data);
}

void CodeAuthenticator::setCode(QString code)
{
    _code = code;
    qDebug()<<code;
    Q_EMIT propertyChanged("code", _code, nullptr);
}


QVariantMap CodeAuthenticator::getObjectData() const
{
    QVariantMap data;
    QVariantMap propCode;
    propCode["data"] = _code;
    data.insert("code", propCode );

    QVariantMap propLength;
    propLength["length"] = _codeLength;
    data.insert("codeLength", propLength );
    return data;
}

IResource::ModificationResult CodeAuthenticator::setProperty(QString name, const QVariant &value, QString token)
{
    Q_UNUSED(token)
    ModificationResult result;
    if(name == "codeLength")
    {
        bool ok;
        quint8 newLength = value.toUInt(&ok);

        if(!ok)
        {
            result.error = IResource::INVALID_PARAMETERS;
            return result;
        }

        _codeLength = newLength;
        QVariantMap data;
        data["data"] = value;
        result.data = value;
        result.error = IResource::NO_ERROR;
        Q_EMIT propertyChanged("codeLength", _codeLength, nullptr);
    }

    result.error = IResource::NOT_SUPPORTED;
    return result;
}

qint16 CodeAuthenticator::codeLength() const
{
    return _codeLength;
}
