#ifndef CODEAUTHENTICATOR_H
#define CODEAUTHENTICATOR_H

#include "Server/Resources/ObjectResource/ObjectResource.h"
#include <QObject>
#include <QTimer>

class CodeAuthenticator : public ObjectResource
{
    Q_OBJECT

public:
    explicit CodeAuthenticator(quint16 codelength, QObject *parent = nullptr);
    explicit CodeAuthenticator(QObject *parent = nullptr);
    void userAuthenticated(QString token);
    void setCode(QString code);
    QVariantMap getObjectData() const override;
    IResource::ModificationResult setProperty(QString name, const QVariant &value, QString token) override;

    qint16 codeLength() const;

private:
    QString _code;
    qint16 _codeLength = 12;

signals:
    void lengthChanged();
};

#endif // CODEAUTHENTICATOR_H
