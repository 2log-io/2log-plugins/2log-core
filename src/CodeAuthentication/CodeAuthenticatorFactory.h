#ifndef CODEAUTHENTICATORFACTORY_H
#define CODEAUTHENTICATORFACTORY_H

#include <QObject>
#include "CodeAuthenticationController.h"
#include "Server/Resources/ResourceManager/IResourceFactory.h"

class CodeAuthenticationFactory : public IResourceFactory
{
    Q_OBJECT

public:
    explicit CodeAuthenticationFactory(CodeAuthenticationController* controller, QObject *parent = nullptr);
    virtual QString getResourceType() const;
    QString getDescriptorPrefix() const;
    resourcePtr createResource(QString token, QString descriptor, QObject *parent);

signals:

private:
    CodeAuthenticationController* _controller;

};

#endif // CODEAUTHENTICATORFACTORY_H
